﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour {

    public int hitPoints = 2;
    public Sprite damageWallSprite;

    private SpriteRenderer spriteRenderer;

	void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();
	}
	public void DamageWall(int damageReceived)
    {
        hitPoints -= damageReceived;
        spriteRenderer.sprite = damageWallSprite;

        if(hitPoints <=0)
        {
            gameObject.SetActive(false);
        }
    }
	
	}
